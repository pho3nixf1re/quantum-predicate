import React, { useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import cc from 'classcat'
import {
  generateDefaultPredicate,
  Predicate,
  PredicateActionName,
  usePredicateReducer,
} from '../../lib/hooks/use-predicate-reducer'
import { generateQuery } from '../../lib/generate-query'
import { Button } from '../Button'
import { Line } from '../Line'
import { SearchPredicate } from '../SearchPredicate'
import { Title } from '../Title'
import styles from './styles.module.scss'

export function App() {
  const { predicates, dispatch } = usePredicateReducer()
  const [query, updateQuery] = useState<string>()
  const handleAdd = () =>
    dispatch({
      type: PredicateActionName.ADD,
      payload: {
        predicate: generateDefaultPredicate(),
      },
    })
  const handleRemove = (predicate: Predicate) =>
    dispatch({
      type: PredicateActionName.REMOVE,
      payload: {
        id: predicate.id,
      },
    })
  const handleChange = (predicate: Predicate) =>
    dispatch({
      type: PredicateActionName.UPDATE,
      payload: { predicate },
    })
  const handleReset = () => {
    dispatch({ type: PredicateActionName.RESET })
    updateQuery('')
  }
  const handleSearch = () => updateQuery(generateQuery(predicates))

  return (
    <div className={styles.container}>
      <Title className={styles.title}>Search for Sessions</Title>
      {predicates.map(predicate => (
        <SearchPredicate
          key={predicate.id}
          onRemove={handleRemove}
          onChange={handleChange}
          predicate={predicate}
        />
      ))}
      <Button onClick={handleAdd} size="small" className={styles.add}>
        And
      </Button>
      <Line className={styles.line} />
      <div className={styles.actions}>
        <Button onClick={handleSearch} className={styles.search}>
          <FontAwesomeIcon icon={faSearch} className={styles.searchIcon} />
          Search
        </Button>
        <Button onClick={handleReset} style="secondary">
          Reset
        </Button>
      </div>
      <pre className={cc([styles.query, { [styles.noQuery]: !query }])}>
        {query || 'Your generated SQL statement goes here.'}
      </pre>
    </div>
  )
}
