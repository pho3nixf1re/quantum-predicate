import React from 'react'
import {
  NumericOperator,
  Predicate,
  PredicateType,
  StringOperator,
} from '../../lib/hooks/use-predicate-reducer'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import { Button } from '../Button'
import { SearchField } from '../../lib/enums'
import styles from './styles.module.scss'

type Props = {
  predicate: Predicate
  onRemove: (predicate: Predicate) => void
  onChange: (predicate: Predicate) => void
}

const FIELD_DISPLAY_NAMES = {
  [SearchField.domain]: 'Domain',
  [SearchField.first_name]: 'First Name',
  [SearchField.last_name]: 'Last Name',
  [SearchField.path]: 'Page Path',
  [SearchField.response_time]: 'Page Response time (ms)',
  [SearchField.screen_height]: 'Screen Height',
  [SearchField.screen_width]: 'Screen Width',
  [SearchField.user_email]: 'User Email',
  [SearchField.visits]: '# of Visits',
}

const STRING_FIELD_PLACEHOLDERS = {
  [SearchField.domain]: 'website.com',
  [SearchField.first_name]: 'John',
  [SearchField.last_name]: 'Doe',
  [SearchField.path]: '/path/to/page',
  [SearchField.response_time]: '0',
  [SearchField.screen_height]: '0',
  [SearchField.screen_width]: '0',
  [SearchField.user_email]: 'john@example.com',
  [SearchField.visits]: '0',
}

export function SearchPredicate({ predicate, onRemove, onChange }: Props) {
  function handleChange(key: string, value: string | string[]) {
    const newPredicate: Predicate = { ...predicate, [key]: value }
    onChange(newPredicate)
  }
  const handleRemove = () => onRemove(predicate)
  const isNumeric = predicate.type == PredicateType.numeric
  const fields: [string, SearchField][] = Object.entries(SearchField).sort()
  const operators = Object.entries(isNumeric ? NumericOperator : StringOperator)

  return (
    <div className={styles.container}>
      <Button
        size="small"
        style="transparent"
        onClick={handleRemove}
        className={styles.remove}
      >
        <FontAwesomeIcon icon={faTimes} />
      </Button>
      <select
        value={predicate.field}
        onChange={event => handleChange('field', event.target.value)}
        className={styles.input}
      >
        {fields.map(([value, key]) => (
          <option key={key + value} value={key}>
            {FIELD_DISPLAY_NAMES[key]}
          </option>
        ))}
      </select>
      {isNumeric && <span className={styles.connector}>is</span>}
      <select
        value={predicate.operator}
        onChange={event => handleChange('operator', event.target.value)}
        className={styles.input}
      >
        {operators.map(([key, value]) => (
          <option key={key + value} value={key}>
            {value}
          </option>
        ))}
      </select>
      <input
        type={isNumeric ? 'number' : 'text'}
        value={predicate.value[0]}
        placeholder={STRING_FIELD_PLACEHOLDERS[predicate.field]}
        onChange={event => {
          const { value } = predicate
          value.splice(0, 1, event.target.value)
          handleChange('value', [...value])
        }}
        className={styles.input}
      />
      {predicate.operator == NumericOperator.between && (
        <>
          <span className={styles.connector}>and</span>
          <input
            type="number"
            value={predicate.value[1]}
            placeholder="0"
            onChange={event => {
              const { value } = predicate
              value.splice(1, 1, event.target.value)
              handleChange('value', [...value])
            }}
            className={styles.input}
          />
        </>
      )}
    </div>
  )
}
