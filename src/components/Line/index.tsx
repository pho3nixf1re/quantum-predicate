import React from 'react'
import cc from 'classcat'
import styles from './styles.module.scss'

type Props = {
  className?: string
}

export const Line = ({ className }: Props) => (
  <hr className={cc([styles.line, className])} />
)
