import React, { ReactNode, MouseEventHandler } from 'react'
import cc from 'classcat'
import styles from './styles.module.scss'

type Props = {
  children?: ReactNode
  className?: string
  onClick?: MouseEventHandler<HTMLButtonElement>
  size?: 'large' | 'small'
  style?: 'primary' | 'secondary' | 'transparent'
}

export const Button = ({
  children,
  className,
  size = 'large',
  style = 'primary',
  onClick,
}: Props) => (
  <button
    className={cc([styles.button, styles[style], styles[size], className])}
    onClick={onClick}
  >
    {children}
  </button>
)
