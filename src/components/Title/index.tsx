import React from 'react'
import styles from './styles.module.scss'

type Props = {
  className?: string
  children: string
}

export const Title = ({ children }: Props) => (
  <h1 className={styles.content}>{children}</h1>
)
