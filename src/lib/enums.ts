export enum SearchField {
  domain = 'domain',
  first_name = 'user_first_name',
  last_name = 'user_last_name',
  path = 'path',
  response_time = 'page_response',
  screen_height = 'screen_height',
  screen_width = 'screen_width',
  user_email = 'user_email',
  visits = 'visits',
}

export enum NumericSearchField {
  response_time = SearchField.response_time,
  screen_height = SearchField.screen_height,
  screen_width = SearchField.screen_width,
  visits = SearchField.visits,
}

export enum StringSearchField {
  domain = SearchField.domain,
  first_name = SearchField.first_name,
  last_name = SearchField.last_name,
  path = SearchField.path,
  user_email = SearchField.user_email,
}
