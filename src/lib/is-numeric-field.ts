import { SearchField, NumericSearchField } from './enums'

export const isNumericField = (field: SearchField): boolean =>
  Object.values(NumericSearchField).includes(field)
