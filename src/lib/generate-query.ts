import { Predicate } from './hooks/use-predicate-reducer'
import { SearchField } from './enums'
import { sequelizePredicate } from './sequelize-predicate'

export function generateQuery(predicates: Predicate[]): string {
  const selectFields = Object.values(SearchField)
    .map(
      (field, index, fields) =>
        `  \`${field}\`${index + 1 == fields.length ? '' : ',\n'}`
    )
    .join('')
  const clauses = predicates
    .map((predicate, index) => {
      const prefix = !index ? '  ' : '  AND '
      return prefix + sequelizePredicate(predicate) + '\n'
    })
    .join('')

  return `
SELECT
${selectFields}
FROM \`session\`
WHERE
${clauses}`
}
