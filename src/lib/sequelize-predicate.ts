import template from 'lodash.template'
import {
  NumericOperator,
  Predicate,
  PredicateType,
  StringOperator,
} from './hooks/use-predicate-reducer'

const NUMERIC_OPERATORS = {
  [NumericOperator.between]: template('between ${value[0]} and ${value[1]}'),
  [NumericOperator.equals]: template('= ${value[0]}'),
  [NumericOperator.greater_than]: template('> ${value[0]}'),
  [NumericOperator.less_than]: template('< ${value[0]}'),
  [NumericOperator.in_list]: template('in [${ value[0] }]'),
}
const STRING_OPERATORS = {
  [StringOperator.contains]: template('like "%${value[0]}%"'),
  [StringOperator.equals]: template('= "${value[0]}"'),
  [StringOperator.in_list]: template(
    'in [<%= value[0].split(",").map(v => `"${v}"`).join() %>]'
  ),
  [StringOperator.starts_with]: template('like "${value[0]}%"'),
}
export function sequelizePredicate(predicate: Predicate): string {
  const { value } = predicate
  let expressionTemplate

  if (predicate.type == PredicateType.numeric) {
    const operator = predicate.operator as NumericOperator
    expressionTemplate = NUMERIC_OPERATORS[operator]
  } else if (predicate.type == PredicateType.string) {
    const operator = predicate.operator as StringOperator
    expressionTemplate = STRING_OPERATORS[operator]
  }

  const expression = expressionTemplate?.({ value })

  return `\`${predicate.field}\` ` + expression
}
