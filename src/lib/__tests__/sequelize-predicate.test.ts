import { PredicateType, StringOperator } from '../hooks/use-predicate-reducer'
import { SearchField } from '../enums'
import { sequelizePredicate } from '../sequelize-predicate'

test('when given a string predicate in list', () => {
  const predicate = {
    id: 'baz',
    type: PredicateType.string,
    field: SearchField.first_name,
    operator: StringOperator.in_list,
    value: ['pow,bam,zap'],
  }

  const result = sequelizePredicate(predicate)

  expect(result).toMatchSnapshot()
})

test('when given a numeric predicate in list', () => {
  const predicate = {
    id: 'baz',
    type: PredicateType.numeric,
    field: SearchField.visits,
    operator: StringOperator.in_list,
    value: ['6,5,3'],
  }

  const result = sequelizePredicate(predicate)

  expect(result).toMatchSnapshot()
})
