import {
  Predicate,
  PredicateType,
  StringOperator,
  NumericOperator,
} from '../hooks/use-predicate-reducer'
import { SearchField } from '../enums'
import { generateQuery } from '../generate-query'

test('when given a list of predicates', () => {
  const predicates: Predicate[] = [
    {
      id: 'foo',
      type: PredicateType.string,
      field: SearchField.user_email,
      operator: StringOperator.equals,
      value: ['zap'],
    },
    {
      id: 'bar',
      type: PredicateType.numeric,
      field: SearchField.response_time,
      operator: NumericOperator.between,
      value: ['3', '6'],
    },
    {
      id: 'baz',
      type: PredicateType.string,
      field: SearchField.first_name,
      operator: StringOperator.equals,
      value: ['pow'],
    },
  ]

  const result = generateQuery(predicates)

  expect(result).toMatchSnapshot()
})
