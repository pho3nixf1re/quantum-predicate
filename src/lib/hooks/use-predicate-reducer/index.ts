import { useReducer } from 'react'
import { v4 as uuid } from 'uuid'
import { SearchField } from '../../enums'
import { reducer } from './reducers'

export enum PredicateActionName {
  ADD,
  REMOVE,
  RESET,
  UPDATE,
}

export enum PredicateType {
  numeric,
  string,
}

export enum NumericOperator {
  equals = 'equals',
  between = 'between',
  greater_than = 'greater than',
  less_than = 'less than',
  in_list = 'in list',
}

export enum StringOperator {
  equals = 'equals',
  contains = 'contains',
  starts_with = 'starts with',
  in_list = 'in list',
}

export type Operator = NumericOperator | StringOperator

export type PredicateAddAction = {
  type: PredicateActionName.ADD
  payload: PredicateAddPayload
}

export type PredicateRemoveAction = {
  type: PredicateActionName.REMOVE
  payload: PredicateRemovePayload
}

export type PredicateResetAction = {
  type: PredicateActionName.RESET
}

export type PredicateUpdateAction = {
  type: PredicateActionName.UPDATE
  payload: PredicateUpdatePayload
}

export type PredicateAction =
  | PredicateAddAction
  | PredicateRemoveAction
  | PredicateResetAction
  | PredicateUpdateAction

export type PredicateAddPayload = {
  predicate: Predicate
}

export type PredicateRemovePayload = {
  id: string
}

export type PredicateUpdatePayload = {
  predicate: Predicate
}

export type Predicate = {
  id: string
  type: PredicateType
  field: SearchField
  operator: Operator
  value: string[]
}

export const generateDefaultPredicate = (): Predicate => ({
  id: uuid(),
  field: SearchField.domain,
  type: PredicateType.string,
  operator: StringOperator.equals,
  value: [''],
})

export const generateInitialState = (): Predicate[] => [
  generateDefaultPredicate(),
]

export function usePredicateReducer(): {
  predicates: Predicate[]
  dispatch: React.Dispatch<PredicateAction>
} {
  const [predicates, dispatch] = useReducer(reducer, generateInitialState())

  return { predicates, dispatch }
}
