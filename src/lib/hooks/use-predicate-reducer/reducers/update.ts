import { isNumericField } from '../../../is-numeric-field'
import {
  NumericOperator,
  Predicate,
  PredicateAction,
  PredicateActionName,
  PredicateType,
  PredicateUpdateAction,
  PredicateUpdatePayload,
  StringOperator,
} from '..'

export function update(
  state: Predicate[],
  payload: PredicateUpdatePayload
): Predicate[] {
  const index = state.findIndex(
    predicate => predicate.id == payload.predicate.id
  )
  const previousPredicate = state[index]
  const { predicate: nextPredicate } = payload
  const isNextNumeric = isNumericField(payload.predicate.field)
  const predicateTypeChange =
    isNumericField(previousPredicate.field) !== isNextNumeric

  if (isNextNumeric) {
    nextPredicate.type = PredicateType.numeric
    if (predicateTypeChange) nextPredicate.operator = NumericOperator.equals
  } else {
    nextPredicate.type = PredicateType.string
    if (predicateTypeChange) nextPredicate.operator = StringOperator.equals
  }

  state[index] = nextPredicate

  return [...state]
}

export function isUpdateAction(
  action: PredicateAction
): action is PredicateUpdateAction {
  return action.type === PredicateActionName.UPDATE
}
