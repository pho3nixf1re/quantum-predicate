import {
  NumericOperator,
  Predicate,
  PredicateType,
  StringOperator,
} from '../..'
import { SearchField } from '../../../../enums'
import { remove } from '../remove'

test('when given a list of predicates', () => {
  const state: Predicate[] = [
    {
      id: 'foo',
      type: PredicateType.string,
      field: SearchField.user_email,
      operator: StringOperator.equals,
      value: [''],
    },
    {
      id: 'bar',
      type: PredicateType.numeric,
      field: SearchField.response_time,
      operator: NumericOperator.between,
      value: ['', ''],
    },
    {
      id: 'baz',
      type: PredicateType.string,
      field: SearchField.first_name,
      operator: StringOperator.equals,
      value: [''],
    },
  ]

  const result = remove(state, { id: 'bar' })

  expect(result).toEqual([
    {
      id: 'foo',
      type: PredicateType.string,
      field: SearchField.user_email,
      operator: StringOperator.equals,
      value: [''],
    },
    {
      id: 'baz',
      type: PredicateType.string,
      field: SearchField.first_name,
      operator: StringOperator.equals,
      value: [''],
    },
  ])
})
