import { Predicate, PredicateType, StringOperator } from '../..'
import { SearchField } from '../../../../enums'
import { add } from '../add'

test('when given a list of predicates', () => {
  const predicate: Predicate = {
    id: 'foo-bar',
    type: PredicateType.string,
    field: SearchField.first_name,
    operator: StringOperator.equals,
    value: [''],
  }

  const result = add([], { predicate })

  expect(result).toEqual([predicate])
})
