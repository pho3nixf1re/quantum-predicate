import {
  NumericOperator,
  Predicate,
  PredicateType,
  StringOperator,
} from '../..'
import { SearchField } from '../../../../enums'
import { update } from '../update'

test('when updating a predicate operator', () => {
  const state: Predicate[] = [
    {
      id: 'foo',
      type: PredicateType.string,
      field: SearchField.user_email,
      operator: StringOperator.equals,
      value: [''],
    },
    {
      id: 'bar',
      type: PredicateType.numeric,
      field: SearchField.response_time,
      operator: NumericOperator.equals,
      value: ['', ''],
    },
    {
      id: 'baz',
      type: PredicateType.string,
      field: SearchField.first_name,
      operator: StringOperator.equals,
      value: [''],
    },
  ]
  const predicate = {
    id: 'bar',
    type: PredicateType.numeric,
    field: SearchField.response_time,
    operator: NumericOperator.between,
    value: ['', ''],
  }

  const result = update(state, { predicate })

  expect(result).toEqual([
    {
      id: 'foo',
      type: PredicateType.string,
      field: SearchField.user_email,
      operator: StringOperator.equals,
      value: [''],
    },
    predicate,
    {
      id: 'baz',
      type: PredicateType.string,
      field: SearchField.first_name,
      operator: StringOperator.equals,
      value: [''],
    },
  ])
})

test('when update changes the predicate field from numeric to string', () => {
  const state: Predicate[] = [
    {
      id: 'foo',
      type: PredicateType.string,
      field: SearchField.user_email,
      operator: StringOperator.equals,
      value: [''],
    },
    {
      id: 'bar',
      type: PredicateType.numeric,
      field: SearchField.response_time,
      operator: NumericOperator.between,
      value: ['', ''],
    },
    {
      id: 'baz',
      type: PredicateType.string,
      field: SearchField.first_name,
      operator: StringOperator.equals,
      value: [''],
    },
  ]
  const predicate = {
    id: 'bar',
    type: PredicateType.numeric,
    field: SearchField.first_name,
    operator: NumericOperator.between,
    value: [''],
  }
  const expected = {
    id: 'bar',
    type: PredicateType.string,
    field: SearchField.first_name,
    operator: StringOperator.equals,
    value: [''],
  }

  const result = update(state, { predicate })

  expect(result).toEqual([
    {
      id: 'foo',
      type: PredicateType.string,
      field: SearchField.user_email,
      operator: StringOperator.equals,
      value: [''],
    },
    expected,
    {
      id: 'baz',
      type: PredicateType.string,
      field: SearchField.first_name,
      operator: StringOperator.equals,
      value: [''],
    },
  ])
})
