import { reset } from '../reset'

test('when given a list of predicates', () => {
  const result = reset()

  expect(result.length).toEqual(1)
})
