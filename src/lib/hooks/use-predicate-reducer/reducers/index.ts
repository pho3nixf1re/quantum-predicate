import { Predicate, PredicateAction } from '..'
import { isAddAction, add } from './add'
import { isRemoveAction, remove } from './remove'
import { isResetAction, reset } from './reset'
import { isUpdateAction, update } from './update'

export function reducer(
  state: Predicate[],
  action: PredicateAction
): Predicate[] {
  if (isAddAction(action)) return add(state, action.payload)
  if (isRemoveAction(action)) return remove(state, action.payload)
  if (isUpdateAction(action)) return update(state, action.payload)
  if (isResetAction(action)) return reset()

  return state
}
