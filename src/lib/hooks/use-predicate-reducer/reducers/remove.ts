import {
  Predicate,
  PredicateRemovePayload,
  PredicateAction,
  PredicateRemoveAction,
  PredicateActionName,
  generateDefaultPredicate,
} from '..'

export function remove(
  state: Predicate[],
  payload: PredicateRemovePayload
): Predicate[] {
  const newState = state.filter(predicate => predicate.id != payload.id)
  if (!newState.length) state.push(generateDefaultPredicate())
  return newState
}

export function isRemoveAction(
  action: PredicateAction
): action is PredicateRemoveAction {
  return action.type === PredicateActionName.REMOVE
}
