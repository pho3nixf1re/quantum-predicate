import {
  Predicate,
  PredicateAction,
  PredicateActionName,
  PredicateAddAction,
  PredicateAddPayload,
} from '..'

export function add(
  state: Predicate[],
  payload: PredicateAddPayload
): Predicate[] {
  return [...state, payload.predicate]
}

export function isAddAction(
  action: PredicateAction
): action is PredicateAddAction {
  return action.type === PredicateActionName.ADD
}
