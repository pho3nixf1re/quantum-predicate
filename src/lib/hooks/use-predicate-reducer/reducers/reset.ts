import {
  generateInitialState,
  Predicate,
  PredicateAction,
  PredicateActionName,
  PredicateResetAction,
} from '..'

export function reset(): Predicate[] {
  return generateInitialState()
}

export function isResetAction(
  action: PredicateAction
): action is PredicateResetAction {
  return action.type === PredicateActionName.RESET
}
